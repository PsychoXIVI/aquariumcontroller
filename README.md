
# AquariumController

This is repository for aquarium controller that I written for my dad.



## Device

Whole project use:

- Arduino Nano (AtMega328p),
- LCD 4x20 HD44780,
- DS18B20 termometers (via OneWire),
- PCF8583 clock (with memory),
- ...



## Goals

- Clock (time/date),
- Configurable options,
- Switchable pages,
- Temperatures,
- Time controlled light (PWM),
- Temperature controlled heating,
- Water ph sensor,
- Bluetooth connection (Android application),
- ...

#### Other

- The code need refactoring (multitasking/scheduler, more modualar composition).
- One day, there should be docs about connections and devices. 


