#pragma once

////////////////////////////////////////////////////////////////////////////////
// Heading

// Library for virtual serial ports
#include <SoftwareSerial.h>

// Bluetooth managed serial port
#define BluetoothPin_RX 7 // Na tym odbiera
#define BluetoothPin_TX 8 // Na tym wysyla
SoftwareSerial BluetoothSerial(BluetoothPin_RX, BluetoothPin_TX);



////////////////////////////////////////////////////////////////////////////////
// Setup

void setupBluetooth()
{
  pinMode(BluetoothPin_RX, INPUT);
  pinMode(BluetoothPin_TX, OUTPUT);
  BluetoothSerial.begin(9600);
}


