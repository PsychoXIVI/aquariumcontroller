
// Simplest placement new implementation
void* operator new(size_t size, void* ptr)
{
    return ptr;
}

// Macro for allocating named buffer and named object reference to it
#define allocateObject(type, name) \
	char name##_buffer[sizeof(type)]; \
	type& name = *((type*) name##_buffer);

// Macro for constructing the object in the allocated buffer
#define constructObject(type, name, ...) \
	new (&name##_buffer) type(\
		__VA_ARGS__	\
	);
