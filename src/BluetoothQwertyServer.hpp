#pragma once

// Library for virtual serial ports
#include <SoftwareSerial.h>

#define DEBUG_BluetoothQwertyServer 1

/// Manages requests and responses utilizing user defined handler. Uses special packet system to prevent data corruption (retransmissions included).
class BluetoothQwertyServer // @note `Qwerty` stands for nothing more than fact there is no better name.
{
	/* Constants */
	
	// My simple packets config
	static constexpr byte headerByte = 59;
	static constexpr byte checksumSeed = 17;
	static constexpr unsigned long retransmissionInterval = 1000;
	
	
	
	/* Fields */

	// Bluetooth managed serial instance
	SoftwareSerial serial; // @TODO . rework `SoftwareSerial`

	// Number to identify recently packets
	byte packetNumber = 0x17;
	
	// Timestamp for next retransmission of currrent packet
	unsigned long retransmissionTimestamp = 0;

	// Output buffer
	#define outputBufferLength 16
	byte outputBuffer[outputBufferLength];
	byte outputBufferIndex = 0;
	byte outputChecksum = 0;

	// Input buffer
	#define inputBufferLength 16
	byte inputBuffer[inputBufferLength];
	byte inputBufferIndex = 0;
	byte inputDataLength = 0;
	
	// Handler
	using Handler_t = void (*)(BluetoothQwertyServer&);
	Handler_t handler;
	
	
	
	/* Operators */
public:
	BluetoothQwertyServer
	(
		Handler_t handler,
		const byte pinRX, 
		const byte pinTX, 
		const uint32_t speed = 9600
	)
	: 	serial(pinRX, pinTX), handler(handler)
	{
		pinMode(pinRX, INPUT);
		pinMode(pinTX, OUTPUT);
		serial.begin(speed);
	}
	
	
	
	/* Methods */
private:
	byte receive()
	{
#ifdef DEBUG_BluetoothQwertyServer
    Serial.print((byte)serial.peek());
		Serial.print(' ');
#endif
		
		// @TODO . atm it is blocking "force" read; Should be either using timeout or non-blocking (state-safe; either state-machine or threads).
		while (serial.available() == 0);
		return serial.read();
	}

	void send(const byte next)
	{
#ifdef DEBUG_BluetoothQwertyServer
		Serial.print(next);
		Serial.print(' ');
#endif
    
    serial.write(next);
	}

public:	
	/// Checks is there more request data available to read
	bool available()
	{
		return inputBufferIndex < inputDataLength;
	}

	/// Reads request data
	byte read()
	{
		if (!available()) {
#ifdef DEBUG_BluetoothQwertyServer
      Serial.println(F("read not available"));
#endif
			return 0;
		}
		
		return inputBuffer[inputBufferIndex++];
	}
  
  byte peek()
  {
    return inputBuffer[inputBufferIndex];
  }
	
	/// Checks for overflow of response data buffer
	bool overflow()
	{
		return (outputBufferIndex >= outputBufferLength);
	}
	
	/// Writes response data
	void write(byte next)
	{
		if (overflow()) {
#ifdef DEBUG_BluetoothQwertyServer
      Serial.println(F("write overflow"));
#endif
			return;
		}
		
		outputBuffer[outputBufferIndex++] = next;
		outputChecksum += next;
	}
	
	void update()
	{
		// (Re)transmission
		if (millis() > retransmissionTimestamp) {
			retransmissionTimestamp = millis() + retransmissionInterval;    
			
			// Send the packet
			if (outputBufferIndex > 0) {
#ifdef DEBUG_BluetoothQwertyServer
				Serial.print(F("RESPONSE: "));
#endif
        
        // Header 
				send(headerByte);
				
				// Packet number
				send(packetNumber);
        send(~packetNumber);
				
				// Length
				send(outputBufferIndex);
				send(~outputBufferIndex);
				
				// Checksum
				send(outputChecksum);
				
				// Data
				for (byte i = 0; i < outputBufferIndex; i++) {
					send(outputBuffer[i]);
				}

#ifdef DEBUG_BluetoothQwertyServer
        Serial.println();
#endif
			}
		}
		
		// Receive the packet
		//	Minimal vaild packet is ACK which is 5 bytes:
		//		- 1: `headerByte` 
		//		- 2, 3: `packetNumber` (with binary negative)
		//		- 4, 5: `messageLength` (with binary negative)
		if (serial.available() >= 5) {
#ifdef DEBUG_BluetoothQwertyServer
      Serial.print(F("REQUEST: "));
#endif
      
			// Header
      while (true) {
        if (receive() == headerByte) {
          break;
        }
        if (serial.available() < 5) {
#ifdef DEBUG_BluetoothQwertyServer
          Serial.println(F("no header"));
#endif
          return;
        }
      } 
			
			// Packet number
			const byte incomingPacketNumber = receive();
			const byte incomingPacketNumberCheck = ~receive();
			if (incomingPacketNumber != incomingPacketNumberCheck) {
#ifdef DEBUG_BluetoothQwertyServer
        Serial.println(F("packet number check failed"));
#endif
        return;
			}
			
			// Length
			inputDataLength = receive();
			const byte inputDataLengthCheck = ~receive();
			if (inputDataLength != inputDataLengthCheck) {
#ifdef DEBUG_BluetoothQwertyServer
        Serial.println(F("input length check failed"));
#endif
				return;
			}
			if (inputDataLength > inputBufferLength) {
#ifdef DEBUG_BluetoothQwertyServer
        Serial.println(F("input length buffer too short"));
#endif
				return;
			}
			
			// Acknowledgement packet condition
			if (inputDataLength == 0) {
#ifdef DEBUG_BluetoothQwertyServer
        Serial.println(F("Acknowledgement"));
#endif
				// Prevent retransmission of acknowledged package
        outputBufferIndex = 0;
        return;
			}
			
			// Prevent double handling same packet
			if (incomingPacketNumber == packetNumber) {
#ifdef DEBUG_BluetoothQwertyServer
        Serial.println(F("packet already handled"));
#endif
				return;
			}
			
			// Receive checksum
			const byte incomingDataChecksum = receive();
			
			// Data
			byte calculatedChecksum = checksumSeed;
			for (byte i = 0; i < inputDataLength; i++) {
				calculatedChecksum += (inputBuffer[i] = receive());
			}
			
			// Validate checksum
			if (calculatedChecksum != incomingDataChecksum) {
#ifdef DEBUG_BluetoothQwertyServer
        Serial.println(F("data checksum failed"));
#endif
				return;
			}
			
			// Update packet number after validating whole incoming packet
			packetNumber = incomingPacketNumber;
			
			// Reset buffer indexes and checksum 
			inputBufferIndex = 0;
			outputBufferIndex = 0;
			outputChecksum = checksumSeed;
			
			// Invoke handler
			handler(*this);
      
      // Ensure sending response
      if (outputBufferIndex == 0) {
        outputBuffer[0] = 0;
        outputBufferIndex = 1;
      }
			
			// Make first transmission.
			retransmissionTimestamp = 0;
      
#ifdef DEBUG_BluetoothQwertyServer
      Serial.println();
#endif
		}
	}
};
