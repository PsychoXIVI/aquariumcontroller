#pragma once

////////////////////////////////////////////////////////////////////////////////
// Heading

// Impulsator
const byte ImpulsatorClockPin = 14;
const byte ImpulsatorDataPin = 15;
byte impulsatorLastState = 0;

// Button
const byte ImpulsatorButton = 4;

// Menu selection
unsigned long menuSelectionEndTime = 0;
#define MenuSelectionTime 3333

// Menu state
enum class MenuLevel {
  None = 0,
  Main,

  // Time/date setting
  TimeHours,
  TimeMinutes,
  DateYears,
  DateMonths,
  DateDays,

  // Temperature view
  TemperatureView,

  // PWM/Lighting
  PWMControllerSelection,
  PWMControllerMenu,
  PWMControllerRisingUp,
  PWMControllerMaxPower,
  PWMControllerOnTimeHours,
  PWMControllerOnTimeMinutes,
  PWMControllerOffTimeHours,
  PWMControllerOffTimeMinutes,
  
  // Heating
  HeatingSettings,
  HeatingSettings_G1_OnTemperature,
  HeatingSettings_G1_OffTemperature,
  HeatingSettings_G1_Alarm,
  HeatingSettings_G2_OnTemperature,
  HeatingSettings_G2_OffTemperature,
  HeatingSettings_G2_Alarm,
  
  Count
};
MenuLevel menuLevel = MenuLevel::None;
byte menuSelection = 0;

byte menuSelectionPWM = 0;
byte menuMaxPowerPercent = 0;

#include "MenuStrings.hpp"

// Predeclare functions
void menuUse();
void menuGo(signed char direction);



////////////////////////////////////////////////////////////////////////////////
// Setup

inline void setupMenu()
{
  pinMode(ImpulsatorClockPin, INPUT);
  pinMode(ImpulsatorDataPin, INPUT);
  pinMode(ImpulsatorButton, INPUT_PULLUP);
}



////////////////////////////////////////////////////////////////////////////////
// Updating

inline void updateMenu()
{
  // Detecting button click
  if (digitalRead(ImpulsatorButton) == LOW) {
    do {
      delay(17);
    }
    while (digitalRead(ImpulsatorButton) == LOW);
    
    // Reset timeout
    menuSelectionEndTime = millis() + MenuSelectionTime;
    
    if (menuLevel != MenuLevel::None) {
      // Use selected menu
      menuUse();
    }
  }
  else {
    // Detecting impulsator rotation
    byte newState = digitalRead(ImpulsatorClockPin);
    if (impulsatorLastState != newState) {
      // Impulsator turned!
      impulsatorLastState = newState;
      
      // Determine direction precisly
      while (true) {
        delay(1);
        
        // Read new state
        newState = digitalRead(ImpulsatorClockPin);

        // Vaild turn detected
        if (impulsatorLastState != newState) {
          // Reset timeout
          menuSelectionEndTime = millis() + MenuSelectionTime;
          
          if (digitalRead(ImpulsatorDataPin) != newState) {
            // Right!
            menuGo(1);
          }
          else {
            // Left!
            menuGo(-1);
          }
          break;
        }
        // Waiting to select
        else {
          // Time to selection // @TODO ? menuSelectionEndTime != 0 - po co to?
          if (menuSelectionEndTime != 0 && menuSelectionEndTime < millis()) {
            break;
          }
        }
      }

      // Renew state
      impulsatorLastState = newState;
    }
    
    // Time to selection
    if (menuSelectionEndTime != 0 && menuSelectionEndTime < millis()) {
      // Reset timeout
      menuSelectionEndTime = millis() + MenuSelectionTime;
      
      if (menuLevel != MenuLevel::None) {
        // Use selected menu
        menuUse();
      }
    }
  }
}


