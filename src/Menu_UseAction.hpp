
/// Dispatches action on confirmed selection/usage of menu entry
void menuUse()
{
#if DEBUG == 1
  // Debug info
  Serial.print("MenuUse -> L: ");
  Serial.print((unsigned int)menuLevel);
  Serial.print("   S: ");
  Serial.print((unsigned int)menuSelection);
  Serial.println(" ");
#endif

  lcd.clear();
  
  switch (menuLevel) {
    ////////////////////////////////////////
    // Main menu
    case MenuLevel::Main:
      switch (menuSelection) {
        case 0:
          menuLevel = MenuLevel::None;
          screenPage = ScreenPage::Clock;
          break;
        case 1:
          menuLevel = MenuLevel::TimeHours;
          screenPage = ScreenPage::Clock;
          break;
        case 2:
          menuLevel = MenuLevel::DateYears;
          screenPage = ScreenPage::Clock;
          break;
        case 3:
          menuLevel = MenuLevel::TemperatureView;
          screenPage = ScreenPage::TemperatureView;
          menuSelection = 0;
          break;
        case 4:
          menuLevel = MenuLevel::PWMControllerSelection;
          menuSelection = 0;
          break;
        case 5:
          menuLevel = MenuLevel::HeatingSettings;
          menuSelection = 0;
          updateMenuStrings_HeatingSettings();
          break;
      }
      break;
    
    ////////////////////////////////////////
    // Time/date setting
    case MenuLevel::TimeHours:
      menuLevel = MenuLevel::TimeMinutes;
      break;
    case MenuLevel::TimeMinutes:
      menuLevel = MenuLevel::None;
      PCF8583_controller.set_time();
      break;
    
    case MenuLevel::DateYears:
      menuLevel = MenuLevel::DateMonths;
      break;
    case MenuLevel::DateMonths:
      menuLevel = MenuLevel::DateDays;
      break;
    case MenuLevel::DateDays:
      menuLevel = MenuLevel::None;
      PCF8583_controller.set_time();
      break;

    ////////////////////////////////////////
    // Temperature
    case MenuLevel::TemperatureView:
      // Nothing == still
      break;
    
    ////////////////////////////////////////
    // PWM/Lighting
    case MenuLevel::PWMControllerSelection:
      if (menuSelection >= 3) {
        menuLevel = MenuLevel::Main;
        menuSelectionPWM = menuSelection;
        menuSelection = 0;
      }
      else {
        menuSelectionPWM = menuSelection;
        menuLevel = MenuLevel::PWMControllerMenu;
        menuSelection = 0;
        
        // Update menu title
        MenuTitle_PWMControllerMenu[PWNControllerMenu_ID_in_title_offset] = menuSelectionPWM + 1 + '0';
        
        menuMaxPowerPercent = (int) (TimedPWNControllers[menuSelectionPWM].settings.maxPower / 2.55);
        
        updateMenuStrings_PWMControllerMenu();
      }
      break;
    
    case MenuLevel::PWMControllerMenu:
      switch (menuSelection) {
        case 0:
          menuLevel = MenuLevel::PWMControllerRisingUp;
          break;
        case 1:
          menuLevel = MenuLevel::PWMControllerMaxPower;
          break;
        case 2:
          menuLevel = MenuLevel::PWMControllerOnTimeHours;
          break;
        case 3:
          menuLevel = MenuLevel::PWMControllerOffTimeHours;
          break;
        case 4:
          menuLevel = MenuLevel::PWMControllerSelection;
          menuSelection = menuSelectionPWM + 1;
          break;
      }
      break;
    case MenuLevel::PWMControllerRisingUp:
      menuLevel = MenuLevel::PWMControllerMenu;
      menuSelection = 1;
      savePWMControllersSettings();
      updateMenuStrings_PWMControllerMenu();
      break;
    case MenuLevel::PWMControllerMaxPower:
      menuLevel = MenuLevel::PWMControllerMenu;
      menuSelection = 2;
      savePWMControllersSettings();
      updateMenuStrings_PWMControllerMenu();
      break;
    case MenuLevel::PWMControllerOnTimeHours:
      menuLevel = MenuLevel::PWMControllerOnTimeMinutes;
      updateMenuStrings_PWMControllerMenu();
      break;
    case MenuLevel::PWMControllerOnTimeMinutes:
      menuLevel = MenuLevel::PWMControllerMenu;
      menuSelection = 3;
      savePWMControllersSettings();
      updateMenuStrings_PWMControllerMenu();
      break;
    case MenuLevel::PWMControllerOffTimeHours:
      menuLevel = MenuLevel::PWMControllerOffTimeMinutes;
      updateMenuStrings_PWMControllerMenu();
      break;
    case MenuLevel::PWMControllerOffTimeMinutes:
      menuLevel = MenuLevel::PWMControllerMenu;
      menuSelection = 4;
      savePWMControllersSettings();
      updateMenuStrings_PWMControllerMenu();
      break;
      
    ////////////////////////////////////////
    // Heating
    case MenuLevel::HeatingSettings:
      switch (menuSelection) {
        case 0:
          menuSelection = 1;
          break;
        case 1:
          menuLevel = MenuLevel::HeatingSettings_G1_OnTemperature;
          break;
        case 2:
          menuLevel = MenuLevel::HeatingSettings_G1_OffTemperature;
          break;
        case 3:
          menuLevel = MenuLevel::HeatingSettings_G1_Alarm;
          break;
        case 4:
          menuSelection = 5;
          break;
        case 5:
          menuLevel = MenuLevel::HeatingSettings_G2_OnTemperature;
          break;
        case 6:
          menuLevel = MenuLevel::HeatingSettings_G2_OffTemperature;
          break;
        case 7:
          menuLevel = MenuLevel::HeatingSettings_G2_Alarm;
          break;
        case 8:
          menuLevel = MenuLevel::Main;
          menuSelection = 0;
          break;
      }
      break;
      
    case MenuLevel::HeatingSettings_G1_OnTemperature: // @TODO . powtarzajacy sie kod...
      menuLevel = MenuLevel::HeatingSettings;
      menuSelection = 2;
      saveHeatingControllersSettings();
      updateMenuStrings_HeatingSettings();
      break;
    case MenuLevel::HeatingSettings_G1_OffTemperature:
      menuLevel = MenuLevel::HeatingSettings;
      menuSelection = 3;
      saveHeatingControllersSettings();
      updateMenuStrings_HeatingSettings();
      break;
    case MenuLevel::HeatingSettings_G1_Alarm:
      menuLevel = MenuLevel::HeatingSettings;
      menuSelection = 5;
      saveHeatingControllersSettings();
      updateMenuStrings_HeatingSettings();
      break;
      
    case MenuLevel::HeatingSettings_G2_OnTemperature:
      menuLevel = MenuLevel::HeatingSettings;
      menuSelection = 6;
      saveHeatingControllersSettings();
      updateMenuStrings_HeatingSettings();
      break;
    case MenuLevel::HeatingSettings_G2_OffTemperature:
      menuLevel = MenuLevel::HeatingSettings;
      menuSelection = 7;
      saveHeatingControllersSettings();
      updateMenuStrings_HeatingSettings();
      break;
    case MenuLevel::HeatingSettings_G2_Alarm:
      menuLevel = MenuLevel::HeatingSettings;
      menuSelection = 8;
      saveHeatingControllersSettings();
      updateMenuStrings_HeatingSettings();
      break;
    
    default:
      // Nothing
      break;
  }
}
