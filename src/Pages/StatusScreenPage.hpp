
{
  // Czas
  lcd.setCursor(0, 0);
  printTimeHHMMSS();
  
  // Timery
  auto printPWMPercent = [](byte id){
    lcd.write(' ');
    const unsigned short percent = static_cast<unsigned short>(TimedPWNControllers[id].currentPower / 2.55);
    lcd.write(percent == 100 ? '1' : ' ');
    lcd.write(percent > 9 ? (char) (percent / 10 % 10 + '0') : ' ');
    lcd.write((char) (percent % 10 + '0'));
    lcd.write('%');
  };
  
  lcd.setCursor(0, 1);
  lcd.print("L1");
  lcd.write(TimedPWNControllers[0].state ? '*' : ' ');
  printPWMPercent(0);
  
  lcd.setCursor(0, 2);
  lcd.print("L2");
  lcd.write(TimedPWNControllers[1].state ? '*' : ' ');
  printPWMPercent(1);
  
  lcd.setCursor(0, 3);
  lcd.print("L3");
  lcd.write(TimedPWNControllers[2].state ? '*' : ' ');
  printPWMPercent(2);
  
  // Grzałki i termometry
  lcd.setCursor(ScreenMaxWidth - 10, 0);
  lcd.print("G1");
  lcd.write(HeatingControllers[0].state ? '*' : ' ');
  lcd.write(temperatureAlarms[0].passedTime > 0 && (millis() % 1000 > 500) ? '!' : ' ');
  printTermometer(0);
  
  lcd.setCursor(ScreenMaxWidth - 10, 1);
  lcd.print("G2");
  lcd.write(HeatingControllers[1].state ? '*' : ' ');
  lcd.write(temperatureAlarms[1].passedTime > 0 && (millis() % 1000 > 500) ? '!' : ' ');
  printTermometer(1);
  
  // Sensor pH wody
  lcd.setCursor(ScreenMaxWidth - 6, 3);
  lcd.print("pH ");
  printphSensor(0);
}
