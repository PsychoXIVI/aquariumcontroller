
/// Draws current menu.
{
  // Select menu data
  const char** menuStrings;
  unsigned short menuLength; 
  const char* menuTitle;
  switch (menuLevel) {
    case MenuLevel::Main:
      menuStrings = MenuStrings_Main;
      menuLength  = MenuLength_Main;
      menuTitle   = MenuTitle_Main;
      break;

    case MenuLevel::PWMControllerSelection:
      menuStrings = MenuStrings_PWMControllerSelection;
      menuLength  = MenuLength_PWMControllerSelection;
      menuTitle   = MenuTitle_PWMControllerSelection;
      break;

    case MenuLevel::PWMControllerMenu:
    case MenuLevel::PWMControllerRisingUp:
    case MenuLevel::PWMControllerMaxPower:
    case MenuLevel::PWMControllerOnTimeHours:
    case MenuLevel::PWMControllerOnTimeMinutes:
    case MenuLevel::PWMControllerOffTimeHours:
    case MenuLevel::PWMControllerOffTimeMinutes:
      menuStrings = MenuStrings_PWMControllerMenu;
      menuLength  = MenuLength_PWMControllerMenu;
      menuTitle   = MenuTitle_PWMControllerMenu;
      break;
    
    case MenuLevel::HeatingSettings:
    case MenuLevel::HeatingSettings_G1_OnTemperature:
    case MenuLevel::HeatingSettings_G1_OffTemperature:
    case MenuLevel::HeatingSettings_G1_Alarm:
    case MenuLevel::HeatingSettings_G2_OnTemperature:
    case MenuLevel::HeatingSettings_G2_OffTemperature:
    case MenuLevel::HeatingSettings_G2_Alarm:
      menuStrings = MenuStrings_HeatingSettings;
      menuLength  = MenuLength_HeatingSettings;
      menuTitle   = MenuTitle_HeatingSettings;
      break;
    
    default:
      return;
      break;
  }
  
  // @TODO , brak lepszego pomyslu gdzie to w tym wszystkim obecnie dac...
  #include "BlinkingStuff.hpp"
  
  // Menu lines
  lcd.setCursor((ScreenMaxWidth - strlen(menuTitle)) / 2, 0); lcd.print(menuTitle);
  for (unsigned short i = 0; i < 4; i++) {
    lcd.setCursor(0, i); 
    lcd.print("*");
    lcd.setCursor(ScreenMaxWidth - 1, i); 
    lcd.print("*");
  }

  // Draw menu entries
  unsigned short c;
  // 1st line
  if (menuSelection - 1 >= 0) {
    c = (ScreenMaxWidth - strlen(menuStrings[menuSelection - 1])) / 2;
    lcd.setCursor(c, 1);
    lcd.print(menuStrings[menuSelection - 1]);
  }
  // 2nd line
  c = (ScreenMaxWidth - strlen(menuStrings[menuSelection])) / 2;
  lcd.setCursor(c, 2);
  lcd.print(menuStrings[menuSelection]);
  // 3rd line
  if (static_cast<unsigned short>(menuSelection + 1) < menuLength) {
    c = (ScreenMaxWidth - strlen(menuStrings[menuSelection + 1])) / 2;
    lcd.setCursor(c, 3);
    lcd.print(menuStrings[menuSelection + 1]);
  }
  
  // Draw selection
  lcd.setCursor(2, 2);
  lcd.print(">");
  lcd.setCursor(ScreenMaxWidth - 3, 2);
  lcd.print("<");
}

