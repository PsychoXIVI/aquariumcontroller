
/// Draws current time (big - 1st and 2nd lines) and date (4th line).
{
  // Hours - hh
  if (menuLevel == MenuLevel::TimeHours && millis() % 1000 > 500) {
    drawBigCharacter('\t', 0, 0);
  }
  else {
    drawBigDigitalTimeHour(0, 0);
  }

  // : (blinking 1s/1s)
  if (millis() % 2000 > 1000) {
    drawBigCharacter(' ', 6, 0);
  }
  else {
    drawBigCharacter(':', 6, 0);
  }

  // Minutes - mm 
  if (menuLevel == MenuLevel::TimeMinutes && millis() % 1000 > 500) {
    drawBigCharacter('\t', 7, 0);
  }
  else {
    drawBigDigitalTimeMinute(7, 0);
  }

  // : (no blinking)
  drawBigCharacter(':', 13, 0);

  // Seconds - ss
  drawBigDigitalTimeSecond(14, 0);

  // Move to 4th line
  lcd.setCursor(0, 3);

  // Day - DD
  if (menuLevel == MenuLevel::DateDays && millis() % 1000 > 500) {
    lcd.print("   ");
  }
  else {
    printDateDay();
    lcd.print(' ');
  }

  // Month - MMM
  if (menuLevel == MenuLevel::DateMonths && millis() % 1000 > 500) {
    lcd.print("    ");
  }
  else {
    printDateMonth();
    lcd.print(' ');
  }

  // Year - YYYY
  if (menuLevel == MenuLevel::DateYears && millis() % 1000 > 500) {
    lcd.print("     ");
  }
  else {
    printDateYear();
    lcd.print(' ');
  }

  // Day of week
  printDateDayOfWeek();
}
