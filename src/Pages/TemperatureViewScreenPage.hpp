
{
  const auto termometerIndex = menuSelection;
  
  // Current temperature
  {
    const String t = String(DS18B20_value[termometerIndex], 1);
    lcd.setCursor(0, 0);
    lcd.write('T');
    lcd.print(termometerIndex + 1);
    lcd.write(' ');
    drawBigCharacter(t[0], 3);
    drawBigCharacter(t[1], 6);
    lcd.setCursor(9, 1);
    lcd.write('.');
    drawBigCharacter(t[3], 10);
    lcd.setCursor(13, 0);
    lcd.write('o');
    drawBigCharacter('C', 14);
  }

  // Temperature range
  {
    lcd.setCursor(0, 3);
    lcd.print("Min ");
    lcd.print(String(HeatingControllers[termometerIndex].settings.minTemperature, 1));
    lcd.setCursor(9, 3);
    lcd.print("Max ");
    lcd.print(String(HeatingControllers[termometerIndex].settings.maxTemperature, 1));
    lcd.setCursor(18, 3);
    lcd.write(0b11011111);
    lcd.write('C');
  }
}
