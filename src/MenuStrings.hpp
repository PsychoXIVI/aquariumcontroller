
// @TODO , dodac PROGMEM gdzie sie da zeby oszczedzic pamiec dynamiczna (i zrobic cos z non-const char*-ami

// Menu strings

/* Main */
const char* MenuTitle_Main = "Menu";
#define MenuLength_Main 6
const char* MenuStrings_Main[MenuLength_Main] = {
      "Powrot", 
    "Ustaw czas ", 
    "Ustaw date ", 
    "Temperatura", 
    "Oswietlenie",
    "Grzalki    "
};

/* PWM/Light selection list */
const char* MenuTitle_PWMControllerSelection = "Wyb. oswietlenie";
#define MenuLength_PWMControllerSelection 4
const char* MenuStrings_PWMControllerSelection[MenuLength_PWMControllerSelection] = {
  "L1 Dzien LED  ", 
  "L2 Dzien Lampa", 
  "L3 Nocne      ", 
       "Powrot"
};

/* PWM/Light settings menu */
/*c*/ char* MenuTitle_PWMControllerMenu = "Ustawienia LN";
#define MenuLength_PWMControllerMenu 5
/*c*/ char* MenuStrings_PWMControllerMenu[MenuLength_PWMControllerMenu] = {
  "PWM Czas XXm", 
  "PWM Moc XXX%", 
  "Wlacz  XX:YY", 
  "Wylacz XX:YY", 
      "Powrot"
};
#define PWNControllerMenu_ID_in_title_offset 12
#define PWMControllerMenu_RisingUp_Index 0
#define PWMControllerMenu_RisingUp_Offset 9
#define PWMControllerMenu_MaxPower_Index 1
#define PWMControllerMenu_MaxPower_Offset 8
#define PWMControllerMenu_OnTime_Index 2
#define PWMControllerMenu_OnTime_Offset 7
#define PWMControllerMenu_OffTime_Index 3
#define PWMControllerMenu_OffTime_Offset 7

/* Heating */
const char* MenuTitle_HeatingSettings = "Ust. temperature";
#define MenuLength_HeatingSettings 9
/*c*/ char* MenuStrings_HeatingSettings[MenuLength_HeatingSettings] = {
   "Grzalka 1",
  "Wlacz  XX,XC",
  "Wylacz XX,XC",
  "Alarm - XXX",
   "Grzalka 2",
  "Wlacz  YY,YC",
  "Wylacz YY,YC",
  "Alarm - YYY",
      "Powrot"
};
#define MenuStrings_HeatingSettings_G1_OnTemperature_Index 1
#define MenuStrings_HeatingSettings_G1_OnTemperature_Offset 7
#define MenuStrings_HeatingSettings_G1_OffTemperature_Index 2
#define MenuStrings_HeatingSettings_G1_OffTemperature_Offset 7
#define MenuStrings_HeatingSettings_G1_AlarmMode_Index 3
#define MenuStrings_HeatingSettings_G1_AlarmMode_Offset 8
#define MenuStrings_HeatingSettings_G2_OnTemperature_Index 5
#define MenuStrings_HeatingSettings_G2_OnTemperature_Offset 7
#define MenuStrings_HeatingSettings_G2_OffTemperature_Index 6
#define MenuStrings_HeatingSettings_G2_OffTemperature_Offset 7
#define MenuStrings_HeatingSettings_G2_AlarmMode_Index 7
#define MenuStrings_HeatingSettings_G2_AlarmMode_Offset 8







void updateMenuStrings_PWMControllerMenu()
{
  // Update rising up time
  MenuStrings_PWMControllerMenu[PWMControllerMenu_RisingUp_Index][PWMControllerMenu_RisingUp_Offset + 0] = 
    (TimedPWNControllers[menuSelectionPWM].settings.risingUpTime > 9 
      ? (char) (TimedPWNControllers[menuSelectionPWM].settings.risingUpTime / 10 + '0') 
      : ' ');
  MenuStrings_PWMControllerMenu[PWMControllerMenu_RisingUp_Index][PWMControllerMenu_RisingUp_Offset + 1] = (char) (TimedPWNControllers[menuSelectionPWM].settings.risingUpTime % 10 + '0');
  
  // Update max PWM power
  MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 0] = (TimedPWNControllers[menuSelectionPWM].settings.maxPower == 255 ? '1' : ' ');
  MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 1] = (menuMaxPowerPercent > 9 ? (char) (menuMaxPowerPercent / 10 % 10 + '0') : ' ');
  MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 2] = (char) (menuMaxPowerPercent % 10 + '0');
  
  // Update on time (hours) in menu strings
  MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 0] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.hour / 10 + '0');
  MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 1] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.hour % 10 + '0');
  // Update on time (minute) in menu strings
  MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 0 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.minute / 10 + '0');
  MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 1 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.minute % 10 + '0');
  
  // Update off time (hour) in menu strings
  MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 0] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.hour / 10 + '0');
  MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 1] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.hour % 10 + '0');
  // Update off time (minute) in menu strings
  MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 0 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.minute / 10 + '0');
  MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 1 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.minute % 10 + '0');
}

void updateMenuStrings_HeatingSettings()
{
  // Update G1 on temperature
  const String g1_onT(HeatingControllers[0].settings.minTemperature, 1);
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OnTemperature_Index][MenuStrings_HeatingSettings_G1_OnTemperature_Offset + 0] = g1_onT[0];
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OnTemperature_Index][MenuStrings_HeatingSettings_G1_OnTemperature_Offset + 1] = g1_onT[1];
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OnTemperature_Index][MenuStrings_HeatingSettings_G1_OnTemperature_Offset + 3] = g1_onT[3];
  
  // Update G1 off temperature
  const String g1_offT(HeatingControllers[0].settings.maxTemperature, 1);
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OffTemperature_Index][MenuStrings_HeatingSettings_G1_OffTemperature_Offset + 0] = g1_offT[0];
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OffTemperature_Index][MenuStrings_HeatingSettings_G1_OffTemperature_Offset + 1] = g1_offT[1];
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_OffTemperature_Index][MenuStrings_HeatingSettings_G1_OffTemperature_Offset + 3] = g1_offT[3];
  
  // Update G1 alarm mode
  if (temperatureAlarms[0].settings.active) {
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 0] = 't';
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 1] = 'a';
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 2] = 'k';
  }
  else {
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 0] = 'n';
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 1] = 'i';
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G1_AlarmMode_Index][MenuStrings_HeatingSettings_G1_AlarmMode_Offset + 2] = 'e';
  }
  
  // Update G2 on temperature
  const String g2_onT(HeatingControllers[1].settings.minTemperature, 1);
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OnTemperature_Index][MenuStrings_HeatingSettings_G2_OnTemperature_Offset + 0] = g2_onT[0];
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OnTemperature_Index][MenuStrings_HeatingSettings_G2_OnTemperature_Offset + 1] = g2_onT[1];
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OnTemperature_Index][MenuStrings_HeatingSettings_G2_OnTemperature_Offset + 3] = g2_onT[3];
  
  // Update G2 off temperature
  const String g2_offT(HeatingControllers[1].settings.maxTemperature, 1);
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OffTemperature_Index][MenuStrings_HeatingSettings_G2_OffTemperature_Offset + 0] = g2_offT[0];
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OffTemperature_Index][MenuStrings_HeatingSettings_G2_OffTemperature_Offset + 1] = g2_offT[1];
  MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_OffTemperature_Index][MenuStrings_HeatingSettings_G2_OffTemperature_Offset + 3] = g2_offT[3];
  
  // Update G2 alarm mode
  if (temperatureAlarms[1].settings.active) {
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 0] = 't';
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 1] = 'a';
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 2] = 'k';
  }
  else {
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 0] = 'n';
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 1] = 'i';
    MenuStrings_HeatingSettings[MenuStrings_HeatingSettings_G2_AlarmMode_Index][MenuStrings_HeatingSettings_G2_AlarmMode_Offset + 2] = 'e';
  }
}
