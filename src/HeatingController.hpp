#pragma once

////////////////////////////////////////////////////////////////////////////////
// Heading

// Uses EEPROM
#include <EEPROM.h>

// Select address of (... @TODO , calc) free bytes of EEPROM to save settings, comment if no saving
#define HeatingSettingsEEPROMAddress 128

// Temperature controlled heating controller class
struct TemperatureHeatingController
{
  /* Fields */
  // Settings 
  struct Settings {
    float minTemperature; 
    float maxTemperature; 
    // @TODO , assert(on < off); ?
  } settings;
  
  // Config 
  const byte pin : 7;
  
  // Current state
  bool state : 1;
  
  /* Operators */
  TemperatureHeatingController (const byte pin)
    : pin(pin)
  {}
  
  /* Methods */
  void setState(const bool value)
  {
    if (state != value) {
      state = value;
      if (value) {
        // ON pin
        digitalWrite(pin, HIGH);
        // Serial.print("G on pin ");
        // Serial.print((int)pin);
        // Serial.println(" -> ON");
      }
      else {
        // OFF pin
        digitalWrite(pin, LOW);
        // Serial.print("G on pin ");
        // Serial.print((int)pin);
        // Serial.println(" -> OFF");
      }
    }
  }
  
  void setup()
  {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
  }
  
  void update(const float currentTemperature)
  {
    if (state) {
      if (currentTemperature > settings.maxTemperature) {
        // Stop heating
        setState(false);
      }
      else {
        // Heating in progress
      }
    }
    else {
      if (currentTemperature < settings.minTemperature) {
        // Lets heat
        setState(true);
      }
      else {
        // Temperature is good
      }
    }
  }
  
  inline void readSettings(unsigned int address)
  {
    EEPROM.get(address, settings);
  }
  inline void saveSettings(unsigned int address)
  {
    EEPROM.put(address, settings);
  }
};

// Number of heating controllers
#define HeatingControllers_Length 2

// Structures constructed with heating controllers pin numbers.
TemperatureHeatingController HeatingControllers[HeatingControllers_Length] = {{16}, {17}};



////////////////////////////////////////////////////////////////////////////////
// Functions

/// Reads heating settings from EEPROM
void readHeatingControllersSettings()
{
  for (unsigned char i = 0; i < HeatingControllers_Length; i++) {
    HeatingControllers[i].readSettings(HeatingSettingsEEPROMAddress + sizeof(TemperatureHeatingController::Settings) * i);
  }
}
/// Saves heating settings to EEPROM
void saveHeatingControllersSettings()
{
  for (unsigned char i = 0; i < HeatingControllers_Length; i++) {
    HeatingControllers[i].saveSettings(HeatingSettingsEEPROMAddress + sizeof(TemperatureHeatingController::Settings) * i);
  }
}



////////////////////////////////////////////////////////////////////////////////
// Setup

inline void setupHeatingControllers()
{
  readHeatingControllersSettings();
  HeatingControllers[0].setup();
  HeatingControllers[1].setup();
}



////////////////////////////////////////////////////////////////////////////////
// Update

inline void updateHeatingControllers()
{
  HeatingControllers[0].update(DS18B20_value[0]);
  HeatingControllers[1].update(DS18B20_value[1]);
}


