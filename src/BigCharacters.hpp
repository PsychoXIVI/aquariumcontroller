#pragma once
/** @file BigCharacters.hpp
 ** @description Provides big characters (3x2) for LiquidCrystal LCD, at least 20x2.
 **/

////////////////////////////////////////////////////////////////////////////////
// Heading

// Bars - custom characters - to construct big characters
byte bar1[8] = { 0b11100, 0b11110, 0b11110, 0b11110, 0b11110, 0b11110, 0b11110, 0b11100 };
byte bar2[8] = { 0b00111, 0b01111, 0b01111, 0b01111, 0b01111, 0b01111, 0b01111, 0b00111 };
byte bar3[8] = { 0b11111, 0b11111, 0b00000, 0b00000, 0b00000, 0b00000, 0b11111, 0b11111 };
byte bar4[8] = { 0b11110, 0b11100, 0b00000, 0b00000, 0b00000, 0b00000, 0b11000, 0b11100 };
byte bar5[8] = { 0b01111, 0b00111, 0b00000, 0b00000, 0b00000, 0b00000, 0b00011, 0b00111 };
byte bar6[8] = { 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b11111, 0b11111 };
byte bar7[8] = { 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00111, 0b01111 };
byte bar8[8] = { 0b11111, 0b11111, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000, 0b00000 };

////////////////////////////////////////////////////////////////////////////////
// Setup

/// Setups bars in LCD driver for big characters 
void setupBigCharacters()
{
	lcd.createChar(1, bar1);
	lcd.createChar(2, bar2);
	lcd.createChar(3, bar3);
	lcd.createChar(4, bar4);
	lcd.createChar(5, bar5);
	lcd.createChar(6, bar6);
	lcd.createChar(7, bar7);
	lcd.createChar(8, bar8);
}

////////////////////////////////////////////////////////////////////////////////
// Drawing

/// Draws big characters
void drawBigCharacter(char c, byte col, byte row = 0) {
	lcd.setCursor(col, row); 
	switch (c) {
		case '0': lcd.write(2);	 lcd.write(8);	lcd.write(1); lcd.setCursor(col, row + 1); lcd.write(2);	lcd.write(6);	 lcd.write(1); break;
		case '1': lcd.write(32); lcd.write(32); lcd.write(1); lcd.setCursor(col, row + 1); lcd.write(32); lcd.write(32); lcd.write(1); break;
		case '2': lcd.write(5);	 lcd.write(3);	lcd.write(1); lcd.setCursor(col, row + 1); lcd.write(2);	lcd.write(6);	 lcd.write(6); break;
		case '3': lcd.write(5);	 lcd.write(3);	lcd.write(1); lcd.setCursor(col, row + 1); lcd.write(7);	lcd.write(6);	 lcd.write(1); break;
		case '4': lcd.write(2);	 lcd.write(6);	lcd.write(1); lcd.setCursor(col, row + 1); lcd.write(32); lcd.write(32); lcd.write(1); break;
		case '5': lcd.write(2);	 lcd.write(3);	lcd.write(4); lcd.setCursor(col, row + 1); lcd.write(7);	lcd.write(6);	 lcd.write(1); break;
		case '6': lcd.write(2);	 lcd.write(3);	lcd.write(4); lcd.setCursor(col, row + 1); lcd.write(2);	lcd.write(6);	 lcd.write(1); break;
		case '7': lcd.write(2);	 lcd.write(8);	lcd.write(1); lcd.setCursor(col, row + 1); lcd.write(32); lcd.write(32); lcd.write(1); break;
		case '8': lcd.write(2);	 lcd.write(3);	lcd.write(1); lcd.setCursor(col, row + 1); lcd.write(2);	lcd.write(6);	 lcd.write(1); break;
		case '9': lcd.write(2);	 lcd.write(3);	lcd.write(1); lcd.setCursor(col, row + 1); lcd.write(7);	lcd.write(6);	 lcd.write(1); break;
		case ':': lcd.print(','); lcd.setCursor(col, row + 1); lcd.print('\''); break;
    case ' ': lcd.print(' '); lcd.setCursor(col, row + 1); lcd.print(' '); break;
    case '\t': lcd.print("      "); lcd.setCursor(col, row + 1); lcd.print("      "); break; // Empty space for 2 big numbers
		case 'C': lcd.write(2);	lcd.write(8);	lcd.setCursor(col, 1); lcd.write(2);	lcd.write(6); break; // Big "C" for temperatures
	}
}

/// Draws 2 digit ig number
inline void drawBigNumber2Digit(byte num, byte col, byte row = 0)
{
  drawBigCharacter('0' + num / 10, col + 0, row);
  drawBigCharacter('0' + num % 10, col + 3, row);
}


