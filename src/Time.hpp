#pragma once

#include "Time_HourMinute.hpp"

// @TODO . zrobic ogolna klase dla czasu
struct Time_hhmmss
{
	/* Fields */
	uint8_t hour;
	uint8_t minute;
  uint8_t second;
	
	/* Operators */
	constexpr Time_hhmmss(uint8_t hour, uint8_t minute, uint8_t second) noexcept
		: hour(hour), minute(minute), second(second)
	{}
  
  /* Methods */
  // template < unit > ;_;
  unsigned long toTimestamp() const 
  {
    return 
    // Seconds
    (
      // Minutes
      (
        // Hours
        (
          7ul * 24ul + // 7 days as bloat time ._.
          static_cast<unsigned long>(hour)
        )
        * 60ul + 
        static_cast<unsigned long>(minute)
      )
      * 60ul +
      static_cast<unsigned long>(second)
    );
  }
};


bool inTimeInterval(const Time_HourMinute& from, const Time_HourMinute& to, const Time_HourMinute& current)
{
	if (from > to) {
		if (from < current) return true;
    if (current <   to) return true;
    return false;
	}
	else {
		if (to  <= current) return false;
    if (current < from) return false;
    return true;
	}
}
