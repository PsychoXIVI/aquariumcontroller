
/// Returns value caped in specified range
template <signed short min, signed short max>
inline short capMenuValue(signed short value)
{
  if (min >= value) return min;
  if (value >= max) return max;
  return value;
}

inline float capMenuValueIn(float min, float max, float value) // C++11 ;_; no float in <tparams>
{
  if (min >= value) return min;
  if (value >= max) return max;
  return value;
}

/// Returns value caped in specified range with overflow
template <signed short min, signed short max>
inline short overflowMenuValue(signed short value)
{
  if (min > value) return value + max;
  if (value > max) return value - max + min;
  return value;
}

/// Dispatches action on changing selection/rotation.
void menuGo(signed char direction)
{
#if DEBUG == 1
  // Debug info
  Serial.print("MenuGo  -> L: ");
  Serial.print((unsigned int)menuLevel);
  Serial.print("   S: ");
  Serial.print((unsigned int)menuSelection);
  Serial.print("   D: ");
  Serial.print(direction);
  Serial.println(" ");
#endif

  lcd.clear();
  
  switch (menuLevel) {
    ////////////////////////////////////////
    // Main menu
    case MenuLevel::None: {
      menuSelection = capMenuValue<0, 2>(direction + menuSelection);
      switch (menuSelection) {
        case 0:
          screenPage = ScreenPage::Clock;
          break;
        
        case 1:
          screenPage = ScreenPage::Status;
          break;
        
        case 2:
          menuLevel = MenuLevel::Main;
          menuSelection = 0;
          screenPage = ScreenPage::Menu;
          break;
          
        default:
          return;
      }
    } break;
    
    case MenuLevel::Main:
      menuSelection = capMenuValue<0, MenuLength_Main - 1>(direction + menuSelection);
      break;
    
    ////////////////////////////////////////
    // Time/date setting
    case MenuLevel::TimeHours:
      PCF8583_controller.hour = overflowMenuValue<0, 23>(PCF8583_controller.hour + direction);
      break;
    case MenuLevel::TimeMinutes:
      PCF8583_controller.minute = overflowMenuValue<0, 59>(PCF8583_controller.minute + direction);
      break;
    
    case MenuLevel::DateYears:
      PCF8583_controller.year += direction;
      break;
    case MenuLevel::DateMonths:
      PCF8583_controller.month = overflowMenuValue<1, 12>(PCF8583_controller.month + direction);
      break;
    case MenuLevel::DateDays:
      PCF8583_controller.day = overflowMenuValue<1, 31>(PCF8583_controller.day + direction);
      break;
    
    ////////////////////////////////////////
    // Temperature
    case MenuLevel::TemperatureView: {
      signed short t = direction + menuSelection;
      if (0 > t) return;
      if (t >= DS18B20_sensors_num) {
        // Last as exit
        menuLevel = MenuLevel::None;
        screenPage = ScreenPage::Clock;
      }
      else {
        menuSelection = t;
      }
    } break;
    
    ////////////////////////////////////////
    // PWM/Lighting
    case MenuLevel::PWMControllerSelection:
      menuSelection = capMenuValue<0, MenuLength_PWMControllerSelection - 1>(direction + menuSelection);
      break;
    
    case MenuLevel::PWMControllerMenu:
      menuSelection = capMenuValue<0, MenuLength_PWMControllerMenu - 1>(direction + menuSelection);
      break;
    
    case MenuLevel::PWMControllerRisingUp:
      TimedPWNControllers[menuSelectionPWM].settings.risingUpTime = overflowMenuValue<0, 60>(direction + TimedPWNControllers[menuSelectionPWM].settings.risingUpTime);
      // Update rising up time
      MenuStrings_PWMControllerMenu[PWMControllerMenu_RisingUp_Index][PWMControllerMenu_RisingUp_Offset + 0] = 
        (TimedPWNControllers[menuSelectionPWM].settings.risingUpTime > 9 
          ? (char) (TimedPWNControllers[menuSelectionPWM].settings.risingUpTime / 10 + '0') 
          : ' ');
      MenuStrings_PWMControllerMenu[PWMControllerMenu_RisingUp_Index][PWMControllerMenu_RisingUp_Offset + 1] = (char) (TimedPWNControllers[menuSelectionPWM].settings.risingUpTime % 10 + '0');
      break;
    
    case MenuLevel::PWMControllerMaxPower:
      menuMaxPowerPercent = overflowMenuValue<0, 100>(direction + menuMaxPowerPercent);
      TimedPWNControllers[menuSelectionPWM].settings.maxPower = (int) (2.55 * menuMaxPowerPercent);
      TimedPWNControllers[menuSelectionPWM].update(Time_hhmmss{(byte)PCF8583_controller.hour, (byte)PCF8583_controller.minute, (byte)PCF8583_controller.second});
      // Update max PWM power
      MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 0] = (TimedPWNControllers[menuSelectionPWM].settings.maxPower == 255 ? '1' : ' ');
      MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 1] = (menuMaxPowerPercent > 9 ? (char) (menuMaxPowerPercent / 10 % 10 + '0') : ' ');
      MenuStrings_PWMControllerMenu[PWMControllerMenu_MaxPower_Index][PWMControllerMenu_MaxPower_Offset + 2] = (char) (menuMaxPowerPercent % 10 + '0');
      break;
    
    case MenuLevel::PWMControllerOnTimeHours:
      TimedPWNControllers[menuSelectionPWM].settings.onTime.hour = overflowMenuValue<0, 23>(TimedPWNControllers[menuSelectionPWM].settings.onTime.hour + direction);
      // Update on time (hours) in menu strings
      MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 0] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.hour / 10 + '0');
      MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 1] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.hour % 10 + '0');
      break;
    case MenuLevel::PWMControllerOnTimeMinutes:
      TimedPWNControllers[menuSelectionPWM].settings.onTime.minute = overflowMenuValue<0, 59>(TimedPWNControllers[menuSelectionPWM].settings.onTime.minute + direction);
      // Update on time (minute) in menu strings
      MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 0 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.minute / 10 + '0');
      MenuStrings_PWMControllerMenu[PWMControllerMenu_OnTime_Index][PWMControllerMenu_OnTime_Offset + 1 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.onTime.minute % 10 + '0');
      break;

    case MenuLevel::PWMControllerOffTimeHours:
      TimedPWNControllers[menuSelectionPWM].settings.offTime.hour = overflowMenuValue<0, 23>(TimedPWNControllers[menuSelectionPWM].settings.offTime.hour + direction);
      // Update off time (hour) in menu strings
      MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 0] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.hour / 10 + '0');
      MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 1] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.hour % 10 + '0');
      break;
    case MenuLevel::PWMControllerOffTimeMinutes:
      TimedPWNControllers[menuSelectionPWM].settings.offTime.minute = overflowMenuValue<0, 59>(TimedPWNControllers[menuSelectionPWM].settings.offTime.minute + direction);
      // Update off time (minute) in menu strings
      MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 0 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.minute / 10 + '0');
      MenuStrings_PWMControllerMenu[PWMControllerMenu_OffTime_Index][PWMControllerMenu_OffTime_Offset + 1 + 3] = (char) (TimedPWNControllers[menuSelectionPWM].settings.offTime.minute % 10 + '0');
      break;
    
    ////////////////////////////////////////
    // Heating
    case MenuLevel::HeatingSettings:
      menuSelection = capMenuValue<0, MenuLength_HeatingSettings - 1>(direction + menuSelection);
      break;
      
    case MenuLevel::HeatingSettings_G1_OnTemperature:
      HeatingControllers[0].settings.minTemperature = capMenuValueIn(20.f, HeatingControllers[0].settings.maxTemperature - 0.1f, HeatingControllers[0].settings.minTemperature + (float)direction/10);
      updateMenuStrings_HeatingSettings();
      break;
    case MenuLevel::HeatingSettings_G1_OffTemperature:
      HeatingControllers[0].settings.maxTemperature = capMenuValueIn(HeatingControllers[0].settings.minTemperature + 0.1f, 32.f, HeatingControllers[0].settings.maxTemperature + (float)direction/10);
      updateMenuStrings_HeatingSettings();
      break;
    
    case MenuLevel::HeatingSettings_G1_Alarm:
      temperatureAlarms[0].settings.active = !(temperatureAlarms[0].settings.active);
      updateMenuStrings_HeatingSettings();
      break;
    
    case MenuLevel::HeatingSettings_G2_OnTemperature:
      HeatingControllers[1].settings.minTemperature = capMenuValueIn(20.f, HeatingControllers[0].settings.maxTemperature - 0.1f, HeatingControllers[1].settings.minTemperature + (float)direction/10);
      updateMenuStrings_HeatingSettings();
      break;
    case MenuLevel::HeatingSettings_G2_OffTemperature:
      HeatingControllers[1].settings.maxTemperature = capMenuValueIn(HeatingControllers[0].settings.minTemperature + 0.1f, 32.f, HeatingControllers[1].settings.maxTemperature + (float)direction/10);
      updateMenuStrings_HeatingSettings();
      break;
    
    case MenuLevel::HeatingSettings_G2_Alarm:
      temperatureAlarms[1].settings.active = !(temperatureAlarms[1].settings.active);
      updateMenuStrings_HeatingSettings();
      break;
    
    default:
      // Nothing
      break;
  }
}
